/*
  ./www/js/app.js
*/

/* -----------------------------------------------------------
    L'AJOUT
   -----------------------------------------------------------
*/

$(function(){

  $('#add').click(function(e){
    e.preventDefault();
    $.ajax({
      url: 'membres/add',
      data: {
              nom: $('#nom').val(),
              prenom: $('#prenom').val(),
              tel: $('#tel').val() ,
              email: $('#email').val()
            },
      method: 'post',
    })
    .done(function(reponsePHP){
      $('#listeDesMembres').prepend(reponsePHP)
                         .find('tr:first')
                         .hide()
                         .slideDown();
      $('#nom').val('');
      $('#prenom').val('');
      $('#tel').val('');
      $('#email').val('');
    })
    .fail(function(){
      alert("Problème durant la transaction !");
    });
  });

/* -----------------------------------------------------------
    SUPRESSION
   -----------------------------------------------------------
*/
  $('#listeDesMembres').on('click','.delete',function(e){
    e.preventDefault();
    $.ajax({
      url: 'membres/' + $(this).closest('tr').attr('data-id') + '/delete',
      context: this
    })
    .done(function(reponsePHP){
      if(reponsePHP == 1){
        $(this).closest('tr').slideUp(function(){ // on fait un remove dans un callback car le remove ne fonctionne pas après un slideUp
          $(this).remove();
        });
      }
    })
    .fail(function(e){
      alert("Problème durant la transaction !");
    });
  });

/* -----------------------------------------------------------
    EDITER - PARTIE FORMULAIRE .edit
   -----------------------------------------------------------
*/
  $('#listeDesMembres').on('click','.edit',function(){
    let elementTexte = $(this).closest('tr').find('.text');
    let contenu = elementTexte.text();
    $(this).each(function(){
      elementTexte.html('<input type="texte" />')
                  .find('input')
                  .val(contenu);
    })

    $(this).toggleClass('edit validate').text('done');
  });

/* -----------------------------------------------------------
    EDITER - PARTIE MODIFICATION EN AJAX .validate
   -----------------------------------------------------------
*/
  $('#listeDesMembres').on('keyup','.validate',function(){
    if(e.keyCode === 13){
      let contenu = $(this).closest('li').find('.text input').val();
      $.ajax({
        url: 'membres/' + $(this).closest('tr').attr('data-id') + '/edit',
        data: {
          nom: $(this).val(),
          prenom: $(this).val(),
          tel: $(this).val() ,
          email: $(this).val()
        },
        method: 'post',
        context: this
      })
       .done(function(reponsePHP){
         $(this).toggleClass('edit validate').text('done');
         let contenu = $(this).val();
         $(this).closest('tr').text(contenu);

       })
       .fail(function(e){
         alert("Problème durant la transaction !");
       });
    }
  });
});
