<?php

/*
 * ./app/routeur.php
 * ROUTEUR PRINCIPAL
 * But :
 *      Charger le controleur et lancer l'action
 *      qui correspond à ce qui est demandé
 *      par l'internaute
 */

use \App\Controleurs\MembresControleur;

if (isset($_GET['membres'])):
  include_once '../app/routeurs/membresRouteur.php';

  /*
    ROUTE PAR DEFAUT
    PATERN:/
    CTRL: membresControleur
    ACTION: index
  */
else:
  include_once '../app/controleurs/membresControleur.php';
  MembresControleur\indexAction($connexion, 1);
endif;
