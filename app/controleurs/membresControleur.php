<?php

/*
 * ./app/controleurs/membresControleur.php
 * Contrôleur des membres
 */

namespace App\Controleurs\MembresControleur;
use \App\Modeles\MembresModele AS Membre;


    function indexAction(\PDO $connexion){
        include_once '../app/modeles/membresModele.php';
        $membres = Membre\findAll($connexion);

        GLOBAL $zoneContenu;
        ob_start();
            include '../app/vues/membres/index.php';
        $zoneContenu = ob_get_clean();
    }

    function addAction(\PDO $connexion, array $data = null){
      // Je demande au modèle d'ajouter le membre
        include_once '../app/modeles/membresModele.php';
        $id = intval(Membre\insertOne($connexion, $data));
        $membre = Membre\findOneById($connexion, $id);

      // Je charge la vue details
        include '../app/vues/membres/details.php';
    }

    function deleteAction(\PDO $connexion, int $id){
      // Je demande au modèle de supprimer le membre
        include_once '../app/modeles/membresModele.php';
        echo Membre\deleteOneById($connexion, $id);
    }

    function editAction(\PDO $connexion, array $data = null ){
      // Je demande au modèle de modifier le membre
        include_once '../app/modeles/membresModele.php';
        echo Membre\updateOneById($connexion, $data);
    }
