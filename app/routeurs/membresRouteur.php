<?php

/*
    ./app/routeurs/membresRouteur.php
 */

  include_once '../app/controleurs/membresControleur.php';
  use \App\Controleurs\MembresControleur;

  switch($_GET['membres']):
    /*
      AJOUT D'UN MEMBRE
      PATERN:/membres/add
      CTRL: membresControleur
      ACTION: addAction
    */
    case 'add':
      MembresControleur\addAction($connexion, [
        'nom'    => $_POST['nom'],
        'prenom' => $_POST['prenom'],
        'tel'    => $_POST['tel'],
        'email'  => $_POST['email']
      ]);
      break;
    /*
    SUPRESSION D'UN MEMBRE
    PATTERN: index.php?membres=delete&id=xxx
    CTRL: membresControleur
    ACTION: deleteAction
    */
    case 'delete':
      MembresControleur\deleteAction($connexion, $_GET['id']);
      break;
    /*
    MODIFICATION D'UN MEMBRE
    PATTERN: index.php?membres=edit&id=xxx
    CTRL: membresControleur
    ACTION: editAction
    */
    case 'edit':
      MembresControleur\editAction($connexion, [
        'id'     => $_GET['id'],
        'nom'    => $_POST['nom'],
        'prenom' => $_POST['prenom'],
        'tel'    => $_POST['tel'],
        'email'  => $_POST['email']
      ]);
      break;
  endswitch;
