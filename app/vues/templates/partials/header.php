<?php
/*
  ./www/vues/template/partials/header.php
  Description:
  Données disponibles :
      -
*/
?>
<div class="navbar-fixed">
        <nav class="grey darken-2">
            <div class="nav-wrapper">
              <a href="#" class="brand-logo right">Backoffice - Societe.be</a>
              <ul id="nav-mobile" class="left">
                <li class="active"><a href="#"><i class="material-icons left">supervisor_account</i>Gestion des membres du personnel</a></li>
                <li><a href="#"><i class="material-icons left">not_interested</i>Autre page de gestion</a></li>
              </ul>
            </div>
          </nav>
</div>
