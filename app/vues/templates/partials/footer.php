<?php
/*
  ./www/vues/template/partials/footer.php
  Description:
  Données disponibles :
      -
*/
?>
 <footer class="page-footer grey darken-3">
   <div class="container">
     <div class="row">
       <div class="col l6 s12">
         <h5 class="white-text">Backoffice - Societe.be</h5>
         <p class="grey-text text-lighten-4">You can use rows and columns here to organize your footer content.</p>
       </div>
       <div class="col l4 offset-l2 s12">
         <h5 class="white-text">Menu</h5>
         <ul>
           <li><a class="grey-text text-lighten-3" href="#">Gestion des membres du personel</a></li>
           <li><a class="grey-text text-lighten-3" href="#">Autre page de gestion</a></li>
         </ul>
       </div>
     </div>
   </div>
   <div class="footer-copyright">
     <div class="container">
     ©2017 IEPS
     <a class="grey-text text-lighten-4 right" href="#!">Site public</a>
     </div>
   </div>
 </footer>
