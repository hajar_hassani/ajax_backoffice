<?php
/*
 * ./app/vues/templates/defaut.php
 * TEMPLATE GENERAL
 * Zones dynamiques :
 *      - zoneTitre
 *      - zoneContenu
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include '../app/vues/templates/partials/head.php'; ?>
    </head>
    <body>
       <!--HEADER-->
        <?php include '../app/vues/templates/partials/header.php'; ?>

        <section id="contenuPrincipal" class="container grey-text text-darken-2">
          <?php echo $zoneContenu; ?>
        </section>

        <!--FOOTER-->
        <?php include '../app/vues/templates/partials/footer.php'; ?>

         <!--SCRIPTS-->
        <?php include '../app/vues/templates/partials/scripts.php'; ?>
    </body>
</html>
