<?php

/*
 * ./app/vues/membres/details.php
 * Variables disponibles
 *      - $membre : ARRAY(id, nom, prenom, tel, email, actif)
 */
?>
<tr data-id="<?php echo $membre['id']; ?>">
    <td><div class="nom text"><?php echo $membre['nom']; ?></div></td>
    <td><div class="prenom text"><?php echo $membre['prenom']; ?></div></td>
    <td><div class="tel text"><?php echo $membre['tel']; ?></div></td>
    <td><div class="email text"><?php echo $membre['email']; ?></div></td>
    <td>
        <div>
            <span><a class="delete red-text" href="#"><i class="material-icons left">indeterminate_check_box</i></a><span>
            <span><a class="edit" href="#"><i class="material-icons">edit</i></a></span>
        </div>
    </td>
</tr>
